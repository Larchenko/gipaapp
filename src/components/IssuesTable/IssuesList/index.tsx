import { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import {
  DragDropContext,
  Droppable,
  Draggable,
  DropResult,
} from 'react-beautiful-dnd';
import { RepoIssue } from 'api/repos/types';
import { RootState } from 'store/types';
import { IssueRow } from './IssueRow';
import { createIdIndexMap, reorder } from './helpers';
import { updateIndexMapAction } from 'store/actions/repos';
import { List } from 'components/common/styles/lists';

function IssuesList(): JSX.Element {
  const dispatch = useDispatch();
  const issues = useSelector<RootState, RepoIssue[]>(
    (state) => state.repos.issues
  );
  const [localIssues, setLocalIssues] = useState(issues);

  function onDragEnd(result: DropResult) {
    if (!result.destination) return;
    if (result.destination.index === result.source.index) return;

    const reordered = reorder(
      localIssues,
      result.source.index,
      result.destination.index
    );
    const indexMap = createIdIndexMap(reordered);

    dispatch(updateIndexMapAction(indexMap));
    setLocalIssues(reordered);
  }

  useEffect(() => {
    setLocalIssues(issues);
  }, [issues]);

  return (
    <DragDropContext onDragEnd={onDragEnd}>
      <Droppable droppableId="droppable">
        {(provided) => (
          <List {...provided.droppableProps} ref={provided.innerRef}>
            {localIssues.map((issue, index) => (
              <Draggable
                key={issue.id}
                draggableId={String(issue.id)}
                index={index}
              >
                {(draggableProvided) => (
                  <div
                    ref={draggableProvided.innerRef}
                    {...draggableProvided.draggableProps}
                    {...draggableProvided.dragHandleProps}
                    style={draggableProvided.draggableProps.style}
                  >
                    <IssueRow issue={issue} />
                  </div>
                )}
              </Draggable>
            ))}
            {provided.placeholder}
          </List>
        )}
      </Droppable>
    </DragDropContext>
  );
}

export { IssuesList };
