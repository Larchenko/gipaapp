import { NumberMap } from 'types/common';

function reorder<T>(
  list: Array<T>,
  startIndex: number,
  endIndex: number
): Array<T> {
  const result = Array.from(list);
  const [removed] = result.splice(startIndex, 1);
  result.splice(endIndex, 0, removed);

  return result;
}

function createIdIndexMap(array: { id: number }[]): NumberMap {
  const idIndexMap = {} as NumberMap;

  array.forEach(({ id }, i) => {
    idIndexMap[id] = i;
  });

  return idIndexMap;
}

export { reorder, createIdIndexMap };
