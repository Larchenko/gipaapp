import styled, { css } from 'styled-components';

const RowListItem = styled.li`
  display: grid;
  grid-template-columns: auto 2fr 1fr 1fr;
  grid-gap: 10px;
  padding: 10px;
  background-color: white;
  border-radius: 6px;
`;

const ColSpan = styled.span`
  display: flex;
  align-items: center;
  ${(p) => css`
    color: ${p.theme.colors.text};
    ${p.theme.fonts.regular};
  `}
`;

export { RowListItem, ColSpan };
