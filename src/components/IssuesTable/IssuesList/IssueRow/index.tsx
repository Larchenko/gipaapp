import { format, formatDistance } from 'date-fns';
import { RepoIssue } from 'api/repos/types';
import { RoundImg } from 'components/common/Avatar/styles';
import { dateFormat } from './config';
import { ColSpan, RowListItem } from './styles';

function IssueRow({ issue }: { issue: RepoIssue }): JSX.Element {
  return (
    <RowListItem>
      <RoundImg src={issue.user.avatar_url} />
      <ColSpan>{issue.title}</ColSpan>
      <ColSpan>{format(new Date(issue.created_at), dateFormat)}</ColSpan>
      <ColSpan>
        {formatDistance(new Date(issue.updated_at), new Date())} ago
      </ColSpan>
    </RowListItem>
  );
}

export { IssueRow };
