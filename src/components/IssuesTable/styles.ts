import styled from 'styled-components';
import { device } from 'config/styles/devices';

const Table = styled.section`
  display: grid;
  grid-template-columns: 1fr;
  grid-gap: 20px;
  width: 100%;
  max-width: 1200px;
  margin: 0 auto;

  @media ${device.tablet} {
    grid-template-columns: 1fr 3fr;
  }
`;

export { Table };
