import { IssuesList } from './IssuesList';
import { ReposList } from './ReposList';
import { Table } from './styles';

function IssuesTable(): JSX.Element {
  return (
    <Table>
      <div>
        <ReposList />
      </div>
      <div>
        <IssuesList />
      </div>
    </Table>
  );
}

export { IssuesTable };
