import { useState, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Repository } from 'api/repos/types';
import { RootState } from 'store/types';
import { fetchRepoIssues } from 'store/thunks/repos';
import { ListItem } from './styles';
import { List } from 'components/common/styles/lists';

function ReposList(): JSX.Element {
  const dispatch = useDispatch();
  const repositories = useSelector<RootState, Repository[]>(
    (state) => state.repos.repositories
  );
  const [selected, setSelected] = useState<string>(repositories[0]?.name);

  useEffect(() => {
    if (selected) dispatch(fetchRepoIssues(selected));
  }, [selected]);

  return (
    <List>
      {repositories.map(({ id, name }) => (
        <ListItem
          key={id}
          selected={selected === name}
          onClick={() => setSelected(name)}
        >
          {name}
        </ListItem>
      ))}
    </List>
  );
}

export { ReposList };
