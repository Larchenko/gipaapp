import styled, { css } from 'styled-components';

const ListItem = styled.li<{ selected?: boolean }>`
  padding: 20px;
  font-size: 18px;
  cursor: pointer;
  border-radius: 6px;

  ${(p) =>
    p.selected
      ? css`
          background-color: ${p.theme.colors.primary};
          color: ${p.theme.colors.reverse};
          ${p.theme.fonts.bold}
        `
      : css`
          background-color: ${p.theme.colors.reverse};
          color: ${p.theme.colors.text};
          ${p.theme.fonts.medium}
        `}

  &:hover {
    transform: scale(1.05);
  }
`;

export { ListItem };
