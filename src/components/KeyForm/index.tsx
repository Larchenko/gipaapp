import { Formik } from 'formik';
import { useCallback } from 'react';
import { useDispatch } from 'react-redux';
import { logInAction } from 'store/actions/user';
import { lss } from 'utils/localStorageService';
import { PrimaryBtn } from 'components/common/styles/buttons';
import { keyFormInitialValues, keyFormValidaionSchema } from './config';
import { StyledForm } from './styles';
import { TextInput } from 'components/common/TextInput';

function KeyForm(): JSX.Element {
  const dispatch = useDispatch();

  const submitForm = useCallback(
    (values) => {
      lss.setAccessToken(values.token);
      dispatch(logInAction());
    },
    [dispatch]
  );

  return (
    <Formik
      onSubmit={submitForm}
      initialValues={keyFormInitialValues}
      validationSchema={keyFormValidaionSchema}
    >
      <StyledForm>
        <TextInput name="token" label="Github personal access token" />
        <PrimaryBtn type="submit">Log In</PrimaryBtn>
      </StyledForm>
    </Formik>
  );
}

export { KeyForm };
