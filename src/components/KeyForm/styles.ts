import { Form } from 'formik';
import styled from 'styled-components';

const StyledForm = styled(Form)`
  display: grid;
  grid-template-columns: 1fr;
  grid-gap: 20px;
  width: 100%;
  max-width: 400px;
`;

export { StyledForm };
