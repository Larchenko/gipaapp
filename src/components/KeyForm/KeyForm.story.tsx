import { KeyForm } from '.';

export function KeyFormExample(): JSX.Element {
  return <KeyForm />;
}

export default { title: 'KeyForm' };
