import * as yup from 'yup';

const keyFormInitialValues = {
  token: '',
};

const keyFormValidaionSchema = yup.object().shape({
  token: yup.string().required(),
});

export { keyFormInitialValues, keyFormValidaionSchema };
