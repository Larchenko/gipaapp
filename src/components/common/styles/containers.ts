import styled from 'styled-components';

const RootContainer = styled.div`
  display: flex;
  flex-direction: column;
  min-height: 100vh;
`;

const PageContainer = styled.div`
  display: flex;
  flex-direction: column;
  flex-grow: 1;
  padding: 40px 20px;
  background-color: ${(p) => p.theme.colors.bg};
`;

const CenterContainer = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  flex-grow: 1;
`;

export { RootContainer, PageContainer, CenterContainer };
