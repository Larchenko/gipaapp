import styled from 'styled-components';

const List = styled.ul<{ dragging?: boolean }>`
  display: grid;
  grid-template-columns: 1fr;
  grid-row-gap: 10px;
  padding: 0;
  list-style-type: none;
`;

export { List };
