import styled, { css } from 'styled-components';
import { device } from 'config/styles/devices';

const H1 = styled.h1`
  padding: 0;
  margin: 0;
  margin-bottom: 20px;
  text-align: center;
  font-size: 36px;
  ${(p) =>
    css`
      color: ${p.theme.colors.primary};
      ${p.theme.fonts.bold};
    `};

  @media ${device.laptop} {
    margin-bottom: 26px;
    font-size: 42px;
  }
`;

export { H1 };
