import styled, { css } from 'styled-components';

const PrimaryBtn = styled.button`
  display: flex;
  justify-content: center;
  align-items: center;
  height: 40px;
  padding: 20px 10px;
  border: none;
  text-align: center;
  color: white;
  font-size: 18px;
  text-transform: uppercase;
  ${(p) => css`
    background-color: ${p.theme.colors.primary};
    border-radius: ${p.theme.borderRadius.default};
    ${p.theme.fonts.medium}

    &:hover {
      transform: scale(1.01);
      box-shadow: ${p.theme.boxShadow.default};
      cursor: pointer;
    }

    &:focus {
      outline: none;
    }

    &:focus-visible {
      outline: 2px solid ${p.theme.colors.secondary};
    }
  `}
`;

export { PrimaryBtn };
