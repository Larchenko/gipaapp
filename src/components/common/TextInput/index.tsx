import { useField } from 'formik';
import { ErrorMsg, InputField, InputGroup, Label } from './styles';
import { TextInputProps } from './types';

function TextInput({ name, label }: TextInputProps): JSX.Element {
  const [field, meta] = useField({
    name,
  });

  return (
    <InputGroup>
      <Label htmlFor={name}>{label}</Label>
      <InputField
        type="text"
        name={name}
        wrong={Boolean(meta.touched && meta.error)}
        value={field.value}
        onChange={field.onChange}
        onBlur={field.onBlur}
      />
      {meta.touched && meta.error && <ErrorMsg>{meta.error}</ErrorMsg>}
    </InputGroup>
  );
}

export { TextInput };
