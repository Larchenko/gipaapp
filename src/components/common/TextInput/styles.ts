import styled, { css } from 'styled-components';

const Label = styled.label`
  font-size: 16px;

  ${(p) => css`
    color: ${p.theme.colors.text};
    ${p.theme.fonts.medium};
  `}
`;

const InputField = styled.input<{ wrong: boolean }>`
  display: flex;
  align-items: center;
  width: 100%;
  height: 40px;

  ${(p) => css`
    border: 2px solid ${p.wrong ? 'red' : p.theme.colors.secondary};
    border-radius: ${p.theme.borderRadius.default};

    &:focus {
      outline: none;
      border: 2px solid ${p.theme.colors.primary};
    }
  `}
`;

const InputGroup = styled.div`
  display: grid;
  grid-template-columns: 1fr;
  grid-gap: 5px;
`;

const ErrorMsg = styled.div`
  color: red;
  ${(p) => p.theme.fonts.regular}

  &::first-letter {
    text-transform: capitalize;
  }
`;

export { InputGroup, ErrorMsg, Label, InputField };
