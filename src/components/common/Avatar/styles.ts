import styled from 'styled-components';

const RoundImg = styled.img`
  width: 42px;
  height: 42px;
  border-radius: 50%;
  border: 2px solid white;
`;

export { RoundImg };
