import { RoundImg } from './styles';

function Avatar({ src }: { src: string }): JSX.Element {
  return <RoundImg src={src} />;
}

export { Avatar };
