import { PageSpinner } from '.';

export function PageSpinnerExample(): JSX.Element {
  return <PageSpinner />;
}

export default { title: 'PageSpinner' };
