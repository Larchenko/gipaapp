import { ThemeConsumer } from 'styled-components';
import { PacmanLoader } from 'react-spinners';
import { CenterContainer } from 'components/common/styles/containers';

function PageSpinner(): JSX.Element {
  return (
    <CenterContainer>
      <ThemeConsumer>
        {(theme) => <PacmanLoader color={theme.colors.primary} />}
      </ThemeConsumer>
    </CenterContainer>
  );
}

export { PageSpinner };
