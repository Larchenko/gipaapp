import axios from 'axios';
import { logOutAction } from '../../store/actions/user';
import { lss } from '../../utils/localStorageService';
import { AxiosConfigFunc } from './types';

const configureAxios: AxiosConfigFunc = function (dispatch) {
  if (configureAxios.configured) return;

  axios.defaults.baseURL = process.env.REACT_APP_API_URL;
  axios.defaults.headers.common['Content-Type'] = 'application/json';

  axios.interceptors.request.use((config) => {
    const token = lss.getAccessToken();
    const newConfig = { ...config };

    if (token) {
      newConfig.headers.Authorization = `Bearer ${token}`;
    } else {
      delete newConfig.headers.Authorization;
    }

    return newConfig;
  });

  axios.interceptors.response.use(
    (response) => {
      return response;
    },
    (error) => {
      if (error.response.status === 401) {
        lss.clearAccessToken();
        dispatch(logOutAction());
      }

      return Promise.reject(error.response);
    }
  );

  configureAxios.configured = true;
};

export { axios, configureAxios };
