import { Dispatch } from '@reduxjs/toolkit';

export interface AxiosConfigFunc {
  (dispatch: Dispatch): void;

  configured?: boolean;
}
