import { axios } from 'api/axios';
import { RepoIssue, Repository } from './types';

const getUserRepos = (): Promise<Repository[]> =>
  axios.get('user/repos').then((res) => res.data);

const getRepoIssues = (owner: string, repo: string): Promise<RepoIssue[]> =>
  axios.get(`repos/${owner}/${repo}/issues`).then((res) => res.data);

export { getUserRepos, getRepoIssues };
