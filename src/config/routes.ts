const ROUTES = Object.freeze({
  home: '/',
  login: '/login',
  notFound: '*',
});

export { ROUTES };
