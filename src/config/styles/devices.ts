const size = Object.freeze({
  tablet: '768px',
  laptop: '1024px',
  desktop: '1440px',
});

const device = Object.freeze({
  tablet: `(min-width: ${size.tablet})`,
  laptop: `(min-width: ${size.laptop})`,
  desktop: `(min-width: ${size.desktop})`,
});

export { device };
