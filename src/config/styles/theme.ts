import { FONTS } from './fonts';
import { COLORS } from './colors';

const THEME = Object.freeze({
  fonts: FONTS,
  colors: COLORS,
  borderRadius: {
    default: '6px',
  },
  boxShadow: {
    default: '0px 4px 20px 0px rgba(54, 39, 16, 0.2)',
  },
});

export { THEME };
