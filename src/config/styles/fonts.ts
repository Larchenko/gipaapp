import { css } from 'styled-components';

const fontsFamilies = Object.freeze({
  roboto: 'Roboto, Arial, sans-serif',
});

const FONTS = Object.freeze({
  regular: css`
    font-family: ${fontsFamilies.roboto};
    font-weight: 400;
  `,

  medium: css`
    font-family: ${fontsFamilies.roboto};
    font-weight: 500;
  `,

  bold: css`
    font-family: ${fontsFamilies.roboto};
    font-weight: 700;
  `,
});

export { FONTS };
