const white = '#fff';
const veryLightGray = '#ddd';
const veryDarkGray = '#333';
const darkCyan = '#177878';
const darkGrayishBlue = '#6c757d';

const COLORS = Object.freeze({
  reverse: white,
  primary: darkCyan,
  bg: veryLightGray,
  text: veryDarkGray,
  secondary: darkGrayishBlue,
});

export { COLORS };
