export interface NumberMap {
  [key: number]: number;
}

export interface IdObject {
  id: number;
}
