class LocalStorageService {
  static instance: LocalStorageService | null = null;

  _accessToken = 'ACCESS_TOKEN';
  _refreshToken = 'REFRESH_TOKEN';

  constructor() {
    if (LocalStorageService.instance) {
      return LocalStorageService.instance;
    }

    LocalStorageService.instance = this;

    return this;
  }

  getAccessToken() {
    return localStorage.getItem(this._accessToken);
  }

  getRefreshToken() {
    return localStorage.getItem(this._refreshToken);
  }

  getTokens() {
    return {
      access: localStorage.getItem(this._accessToken),
      refresh: localStorage.getItem(this._refreshToken),
    };
  }

  setAccessToken(token: string) {
    return localStorage.setItem(this._accessToken, token);
  }

  setRefreshToken(token: string) {
    return localStorage.setItem(this._refreshToken, token);
  }

  setTokens({ access, refresh }: { access: string; refresh: string }) {
    return {
      access: localStorage.setItem(this._accessToken, access),
      refresh: localStorage.setItem(this._refreshToken, refresh),
    };
  }

  clearAccessToken() {
    return localStorage.removeItem(this._accessToken);
  }

  clearRefreshToken() {
    return localStorage.removeItem(this._refreshToken);
  }

  clearTokens() {
    return {
      access: localStorage.removeItem(this._accessToken),
      refresh: localStorage.removeItem(this._refreshToken),
    };
  }
}

const lss = new LocalStorageService();

export { lss };
