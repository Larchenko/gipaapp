import React, { Suspense, lazy, useEffect } from 'react';
import { useDispatch } from 'react-redux';
import { Route, Switch } from 'react-router-dom';
import { configureAxios } from 'api/axios';
import { PageContainer } from 'components/common/styles/containers';
import { PageSpinner } from 'components/common/PageSpinner';
import { ROUTES } from 'config/routes';
import { logInAction, logOutAction } from 'store/actions/user';
import { lss } from 'utils/localStorageService';
import { AnonRoute } from './AnonRoute';
import { PrivateRoute } from './PrivateRoute';

const Home = lazy(() => import('pages/Home'));
const Login = lazy(() => import('pages/Login'));
const NotFound = lazy(() => import('pages/NotFound'));

function Routes(): JSX.Element {
  const dispatch = useDispatch();

  configureAxios(dispatch);

  useEffect(() => {
    if (lss.getAccessToken()) dispatch(logInAction());
    else dispatch(logOutAction());
  }, []);

  return (
    <PageContainer>
      <Suspense fallback={<PageSpinner />}>
        <Switch>
          <PrivateRoute exact path={ROUTES.home} component={Home} />
          <AnonRoute path={ROUTES.login} component={Login} />
          <Route path={ROUTES.notFound} component={NotFound} />
        </Switch>
      </Suspense>
    </PageContainer>
  );
}

export { Routes };
