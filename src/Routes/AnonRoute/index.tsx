import { useSelector } from 'react-redux';
import { Redirect, Route, RouteProps } from 'react-router';
import { ROUTES } from 'config/routes';
import { RootState } from 'store/types';

function AnonRoute({ path, exact, component }: RouteProps): JSX.Element {
  const authenitcated = useSelector<RootState>(
    (state) => state.user.authenticated
  );

  return authenitcated ? (
    <Redirect to={ROUTES.home} />
  ) : (
    <Route path={path} exact={exact} component={component} />
  );
}

export { AnonRoute };
