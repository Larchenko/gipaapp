import { useSelector } from 'react-redux';
import { Redirect, Route, RouteProps } from 'react-router';
import { ROUTES } from 'config/routes';
import { RootState } from 'store/types';

function PrivateRoute({ path, exact, component }: RouteProps): JSX.Element {
  const authenitcated = useSelector<RootState>(
    (state) => state.user.authenticated
  );

  return authenitcated ? (
    <Route path={path} exact={exact} component={component} />
  ) : (
    <Redirect to={ROUTES.login} />
  );
}

export { PrivateRoute };
