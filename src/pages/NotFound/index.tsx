import { CenterContainer } from 'components/common/styles/containers';
import { H1 } from 'components/common/styles/headings';

function NotFound(): JSX.Element {
  return (
    <CenterContainer>
      <H1>Oops! Page not found...</H1>
    </CenterContainer>
  );
}

export default NotFound;
