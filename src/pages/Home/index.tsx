import { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { CenterContainer } from 'components/common/styles/containers';
import { PageSpinner } from 'components/common/PageSpinner';
import { RootState } from 'store/types';
import { fetchUserRepos } from 'store/thunks/repos';
import { IssuesTable } from 'components/IssuesTable';
import { H1 } from 'components/common/styles/headings';

function Home(): JSX.Element {
  const dispatch = useDispatch();
  const loading = useSelector<RootState, boolean>(
    (state) => state.repos.loading
  );

  useEffect(() => {
    dispatch(fetchUserRepos());
  }, []);

  if (loading) return <PageSpinner />;

  return (
    <CenterContainer>
      <H1>Issues planning</H1>
      <IssuesTable />
    </CenterContainer>
  );
}

export default Home;
