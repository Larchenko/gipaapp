import { CenterContainer } from 'components/common/styles/containers';
import { H1 } from 'components/common/styles/headings';
import { KeyForm } from 'components/KeyForm';

function Login(): JSX.Element {
  return (
    <CenterContainer>
      <H1>Welcome</H1>
      <KeyForm />
    </CenterContainer>
  );
}

export default Login;
