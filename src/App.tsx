import { BrowserRouter } from 'react-router-dom';
import { ThemeProvider } from 'styled-components';
import { Provider } from 'react-redux';
import { PersistGate } from 'redux-persist/integration/react';
import { THEME } from './config/styles/theme';
import { Routes } from './Routes';
import { GlobalStyles } from './components/GlobalStyles';
import { persistor, store } from './store';
import { RootContainer } from './components/common/styles/containers';
import { PageSpinner } from './components/common/PageSpinner';

function App(): JSX.Element {
  return (
    <BrowserRouter>
      <ThemeProvider theme={THEME}>
        <GlobalStyles />
        <PersistGate loading={<PageSpinner />} persistor={persistor}>
          <Provider store={store}>
            <RootContainer>
              <Routes />
            </RootContainer>
          </Provider>
        </PersistGate>
      </ThemeProvider>
    </BrowserRouter>
  );
}

export default App;
