import { ReposState } from './reducers/repos/types';
import { UserState } from './reducers/user/types';

export interface MiddlewareConfig {
  serializableCheck: {
    ignoredActions: string[];
  };
}

export interface RootState {
  user: UserState;
  repos: ReposState;
}
