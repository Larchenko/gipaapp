import { createAction } from '@reduxjs/toolkit';
import { RepoIssue, Repository } from 'api/repos/types';
import { NumberMap } from 'types/common';

const toggleReposLoadingFlagAction = createAction<boolean>(
  'TOGGLE_REPOS_LOADING_FLAG'
);
const toggleIssuesLoadingFlagAction = createAction<boolean>(
  'TOGGLE_ISSUES_LOADING_FLAG'
);
const putUserReposAction = createAction<Repository[]>('PUT_USER_REPOS');
const putRepoIssuesAction = createAction<RepoIssue[]>('PUT_REPOS_ISSUES');
const updateIndexMapAction = createAction<NumberMap>('UPDATE_INDEX_MAP');

export {
  putUserReposAction,
  putRepoIssuesAction,
  updateIndexMapAction,
  toggleReposLoadingFlagAction,
  toggleIssuesLoadingFlagAction,
};
