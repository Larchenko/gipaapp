import { createAction } from '@reduxjs/toolkit';

const logInAction = createAction('LOG_IN');
const logOutAction = createAction('LOG_OUT');

export { logInAction, logOutAction };
