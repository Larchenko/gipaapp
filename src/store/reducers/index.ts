import { combineReducers } from '@reduxjs/toolkit';
import { userReducer } from './user';
import { reposReducer } from './repos';

export const rootReducer = combineReducers({
  user: userReducer,
  repos: reposReducer,
});
