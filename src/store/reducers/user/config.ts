import { UserState } from './types';

export const userInitialState: UserState = {
  authenticated: false,
};
