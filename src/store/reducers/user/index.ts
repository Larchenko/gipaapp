import { createReducer } from '@reduxjs/toolkit';
import { userInitialState } from './config';
import { UserState } from './types';
import { logInAction, logOutAction } from 'store/actions/user';

const userReducer = createReducer<UserState>(userInitialState, {
  [logInAction.type]: (state) => {
    state.authenticated = true;
  },
  [logOutAction.type]: (state) => {
    state.authenticated = false;
  },
});

export { userReducer };
