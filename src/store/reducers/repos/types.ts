import { RepoIssue, Repository } from 'api/repos/types';
import { NumberMap } from 'types/common';

export interface ReposState {
  loading: boolean;
  loadingIssues: boolean;
  issues: RepoIssue[];
  issuesIndexMap: NumberMap;
  repositories: Repository[];
}
