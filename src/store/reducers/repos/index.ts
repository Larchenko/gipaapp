import { createReducer, PayloadAction } from '@reduxjs/toolkit';
import { reposInitialState } from './config';
import { ReposState } from './types';
import {
  putRepoIssuesAction,
  putUserReposAction,
  toggleIssuesLoadingFlagAction,
  toggleReposLoadingFlagAction,
  updateIndexMapAction,
} from 'store/actions/repos';
import { RepoIssue, Repository } from 'api/repos/types';
import { NumberMap } from 'types/common';
import { sortByIndexMap } from './helpers';

const reposReducer = createReducer<ReposState>(reposInitialState, {
  [toggleReposLoadingFlagAction.type]: (
    state,
    action: PayloadAction<boolean>
  ) => {
    state.loading = action.payload;
  },
  [putUserReposAction.type]: (state, action: PayloadAction<Repository[]>) => {
    state.repositories = action.payload;
  },
  [toggleIssuesLoadingFlagAction.type]: (
    state,
    action: PayloadAction<boolean>
  ) => {
    state.loadingIssues = action.payload;
  },
  [putRepoIssuesAction.type]: (state, action: PayloadAction<RepoIssue[]>) => {
    state.issues = sortByIndexMap(action.payload, state.issuesIndexMap);
  },
  [updateIndexMapAction.type]: (state, action: PayloadAction<NumberMap>) => {
    state.issuesIndexMap = { ...state.issuesIndexMap, ...action.payload };
  },
});

export { reposReducer };
