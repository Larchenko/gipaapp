import { ReposState } from './types';

export const reposInitialState: ReposState = {
  loading: true,
  loadingIssues: true,
  issues: [],
  issuesIndexMap: {},
  repositories: [],
};
