import { IdObject, NumberMap } from 'types/common';

function sortByIndexMap<T>(
  array: T & IdObject[],
  indexMap: NumberMap
): T & IdObject[] {
  return array.sort((current, next) => {
    if (Number(+indexMap[current.id] < +indexMap[next.id])) return -1;
    if (Number(+indexMap[current.id] > +indexMap[next.id])) return 1;

    return 0;
  });
}

export { sortByIndexMap };
