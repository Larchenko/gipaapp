import { persistStore, persistReducer } from 'redux-persist';
import { configureStore, getDefaultMiddleware } from '@reduxjs/toolkit';
import { enableBatching } from 'redux-batched-actions';
import { rootReducer } from './reducers';
import { isDevEnv } from 'utils/enviroment';
import { MiddlewareConfig, RootState } from './types';
import { ignoredActions, persistConfig } from './config';

const persistedReducer = persistReducer(
  persistConfig,
  enableBatching(rootReducer)
);

const store = configureStore({
  reducer: persistedReducer,
  devTools: isDevEnv(),
  middleware: getDefaultMiddleware<RootState, MiddlewareConfig>({
    serializableCheck: {
      ignoredActions,
    },
  }),
});

const persistor = persistStore(store);

export { store, persistor };
