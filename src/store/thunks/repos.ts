import { Dispatch } from '@reduxjs/toolkit';
import { RootState } from 'store/types';
import { getRepoIssues, getUserRepos } from 'api/repos';
import {
  putUserReposAction,
  putRepoIssuesAction,
  toggleReposLoadingFlagAction,
  toggleIssuesLoadingFlagAction,
} from 'store/actions/repos';

const fetchUserRepos = () => async (dispatch: Dispatch): Promise<void> => {
  dispatch(toggleReposLoadingFlagAction(true));
  try {
    const data = await getUserRepos();
    dispatch(putUserReposAction(data));
  } catch (e) {
    console.error(e);
  } finally {
    dispatch(toggleReposLoadingFlagAction(false));
  }
};

const fetchRepoIssues = (repo: string) => async (
  dispatch: Dispatch,
  getState: () => RootState
): Promise<void> => {
  const {
    repos: { repositories },
  } = getState();

  const owner = repositories.find(({ name }) => name === repo)?.owner?.login;

  if (owner) {
    dispatch(toggleIssuesLoadingFlagAction(true));
    try {
      const data = await getRepoIssues(owner, repo);
      dispatch(putRepoIssuesAction(data));
    } catch (e) {
      console.error(e);
    } finally {
      dispatch(toggleIssuesLoadingFlagAction(false));
    }
  }
};

export { fetchUserRepos, fetchRepoIssues };
