import { Provider } from 'react-redux';
import { ThemeProvider } from 'styled-components';
import { addDecorator } from '@storybook/react';
import StoryRouter from 'storybook-react-router';
import { THEME } from '../src/config/styles/theme';
import { GlobalStyles } from '../src/components/GlobalStyles';
import {
  CenterContainer,
  PageContainer,
} from '../src/components/common/styles/containers';
import { store } from '../src/store';

addDecorator((story) => (
  <Provider store={store}>
    <ThemeProvider theme={THEME}>
      <GlobalStyles />
      <PageContainer>
        <CenterContainer>{story()}</CenterContainer>
      </PageContainer>
    </ThemeProvider>
  </Provider>
));

addDecorator(StoryRouter());
