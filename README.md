## Gipapp - Github issues planning app

[Single page application](https://en.wikipedia.org/wiki/Single-page_application)
with
[predictable state container](https://redux.js.org/introduction/getting-started)
and [css-in-js styling technique](https://en.wikipedia.org/wiki/CSS-in-JS),
bootstrapped by [CRA](https://create-react-app.dev/docs/getting-started/)

### Core libraries

1. [React.js](https://reactjs.org/)
2. [Redux](https://redux.js.org/)
3. [Axios](https://github.com/axios/axios)
4. [Styled Components](https://styled-components.com/)
5. [Formik](https://formik.org/docs/overview)
6. [Redux Persist](https://github.com/rt2zz/redux-persist)
7. [Typescript](https://www.typescriptlang.org/)

### Code quality tools

1. [ESLint](https://eslint.org/) with
   [airbnb config](https://github.com/airbnb/javascript)
2. [Prettier](https://prettier.io/)
3. [Husky](https://github.com/typicode/husky#readme)

## Getting started

### Requirements

1. [Node.js](https://nodejs.org/en/) v12.16.3
2. [yarn](https://yarnpkg.com/) v1.22.5

### To run locally

1. clone project
2. `yarn`
3. `yarn start`

### Basic scripts

`yarn start` Runs the app in the development mode. Open
[http://localhost:3000](http://localhost:3000) to view it in the browser.

`yarn build` Builds the app for production to the `build` folder.

`yarn run-stories` Runs storybook at [http://localhost:3001](http://localhost:3001)

## Author notes

1. My goal was not just to complete the project, but to show what a large scalable project should look like.
2. I didn’t have time to refactor some components.
3. I can describe API during personal converstion XD

### Lead time

Approximately 12 hours

### API description

After each move of a task, send a POST request with a ID-index map (look at repository reducer store/reducers/repos).
